package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"uu_go_user_service/config"
	"uu_go_user_service/genproto/user_service"
	"uu_go_user_service/grpc/client"
	"uu_go_user_service/models"
	"uu_go_user_service/pkg/logger"
	"uu_go_user_service/storage"
)

type HobbyService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedHobbyServiceServer
}

func NewHobbyService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *HobbyService {
	return &HobbyService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *HobbyService) Create(ctx context.Context, req *user_service.CreateHobby) (resp *user_service.Hobby, err error) {

	i.log.Info("---CreateHobby------>", logger.Any("req", req))

	pKey, err := i.strg.Hobby().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateHobby->Hobby->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Hobby().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyHobby->Hobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *HobbyService) GetByID(ctx context.Context, req *user_service.HobbyPrimaryKey) (resp *user_service.Hobby, err error) {

	i.log.Info("---GetHobbyByID------>", logger.Any("req", req))

	resp, err = i.strg.Hobby().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetHobbyByID->Hobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *HobbyService) GetList(ctx context.Context, req *user_service.GetListHobbyRequest) (resp *user_service.GetListHobbyResponse, err error) {

	i.log.Info("---GetHobbies------>", logger.Any("req", req))

	resp, err = i.strg.Hobby().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetHobby->Hobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *HobbyService) Update(ctx context.Context, req *user_service.UpdateHobby) (resp *user_service.Hobby, err error) {

	i.log.Info("---UpdateHobby------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Hobby().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateHobby--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Hobby().GetByPKey(ctx, &user_service.HobbyPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetHobby->Hobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *HobbyService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchHobby) (resp *user_service.Hobby, err error) {

	i.log.Info("---UpdatePatchEnrolledHobby------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Hobby().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchHobby--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Hobby().GetByPKey(ctx, &user_service.HobbyPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetHobby->Hobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *HobbyService) Delete(ctx context.Context, req *user_service.HobbyPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteHobby------>", logger.Any("req", req))

	err = i.strg.Hobby().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteHobby->Hobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
