package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"uu_go_user_service/config"
	"uu_go_user_service/genproto/user_service"
	"uu_go_user_service/grpc/client"
	"uu_go_user_service/grpc/service"
	"uu_go_user_service/pkg/logger"
	"uu_go_user_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))

	user_service.RegisterHobbyServiceServer(grpcServer, service.NewHobbyService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
